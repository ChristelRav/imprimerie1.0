package imprimerie;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseObject {
    public ResultSet executeQuerry(Connection connection, String querry) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Statement statement=connection.createStatement();
        ResultSet resultSet= statement.executeQuery(querry);
        statement.close();
        return resultSet;
    }
    public static void executeQuerryVoid(Connection connection, String querry) throws SQLException {
        Statement statement=connection.createStatement();
        statement.executeQuery(querry);
        statement.close();
    }
    public static void executeUpdate(Connection connection, String querry) throws SQLException {
        Statement statement= connection.createStatement();
        statement.executeUpdate(querry);
        statement.close();
    }
    public List<Object> select(String querry,Connection connection, int limit) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, SQLException {
        Statement statement=connection.createStatement();
        ResultSet resultSet= statement.executeQuery(querry);
        Field[] fields=this.getClass().getDeclaredFields();
        Object newObject = this.getClass().getConstructor().newInstance();
        List<Object> newList=new ArrayList<>();
        int initial=0;
        while(resultSet.next() && initial!=limit) {
            for (Field field : fields) {
                Method objectSetter = this.getClass().getDeclaredMethod("set" + uppercaseFirstLetter(field.getName()), field.getType());
                Method resultGetType = ResultSet.class.getDeclaredMethod("get" + uppercaseFirstLetter(field.getType().getSimpleName()), String.class);
                objectSetter.invoke(newObject, resultGetType.invoke(resultSet, field.getName()));
                           }
            newList.add(newObject);
            initial++;

        }
        statement.close();
        return newList;
    }
    public static String uppercaseFirstLetter(String toUppercase)
    {
        return (toUppercase.substring(0,1)).toUpperCase()+toUppercase.substring(1,toUppercase.length());
    }

    public static String formatMybddDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String d="";
        d = "'" + dateFormat.format(date) + "'";
        return d;
    }
    public  String formatvaluesfromobject() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String values= new String();
        Field[] field=this.getClass().getDeclaredFields();

        for(int i=1; i< field.length; i++)
        {
            String insert="";
            if(field[i].getType()==String.class)
            {
                insert="'"+this.getClass().getMethod("get"+uppercaseFirstLetter(field[i].getName())).invoke(this)+"'";
            }
            else if (field[i].getType()== java.sql.Date.class) {
                insert=formatMybddDate((Date)this.getClass().getMethod("get"+uppercaseFirstLetter(field[i].getName())).invoke(this));

            } else{
                insert=String.valueOf(this.getClass().getMethod("get"+uppercaseFirstLetter(field[i].getName())).invoke(this));
            }

            if(i< field.length-1)
            {
                insert=insert+",";
            }
            values=values+insert;
        }
        return values;
    }
    public String formatAttributes()
    {
        Field [] fields=this.getClass().getDeclaredFields();
        String t=" (";
        for (int i = 1; i < fields.length; i++) {
            t=t+fields[i].getName()+",";
        }
        t=t.substring(0, t.length()-1);
        t=t+") ";
        return t;
    }
    public void save(Connection connection) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, SQLException {
        String querryUpdate="insert into "+this.getClass().getSimpleName()+this.formatAttributes()+" values ("+this.formatvaluesfromobject()+")";
        System.out.println(querryUpdate);
        executeUpdate(connection,querryUpdate);
    }
    public  Object get(Connection connection, String condition) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String querrySelected="select * from "+this.getClass().getSimpleName()+ " where "+ condition + "limit 1";
        List<Object> obj=select(querrySelected,connection,1);
        return obj.get(0);
    }
    public List<Object> getList(Connection connection, String condition) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String querrySelected="select * from "+this.getClass().getSimpleName()+ " where "+ condition  ;
        return select(querrySelected,connection,-1);
    }
    public List<Object> getList(Connection connection) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        List<Object> object=getList(connection,"1=1");
        return object;
    }
//    List<Object> getList()
}

