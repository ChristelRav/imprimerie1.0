package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class ActionMateriel {
    int idaction , idmateriel , quantite;
    public int getIdaction() {
        return idaction;
    }
    public void setIdaction(int idaction) {
        this.idaction = idaction;
    }
    public int getIdmateriel() {
        return idmateriel;
    }
    public void setIdmateriel(int idmateriel) {
        this.idmateriel = idmateriel;
    }
    public int getQuantite() {
        return quantite;
    }
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    public ActionMateriel() {}
    public ActionMateriel(int idaction, int idmateriel, int quantite) {
        this.idaction = idaction;
        this.idmateriel = idmateriel;
        this.quantite = quantite;
    }
    public Vector<ActionMateriel> getAllActionMateriel(Connection c ){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<ActionMateriel> all = new Vector<ActionMateriel>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from ActionMateriel");
            while(res.next() ){
                int idN=res.getInt(1);
                int idS=res.getInt(2);
                int ht = res.getInt(3);
                all.add(new ActionMateriel(idN,idS,ht));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }


    public ActionMateriel getOneActionMateriel(Connection c,int idAction)throws Exception{
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            ActionMateriel all = new ActionMateriel();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from  ActionMateriel where idAction ="+idAction+"");
            while(res.next() ){
                int idE=res.getInt(1);       int idN=res.getInt(2);      int idG=res.getInt(3);
                all = new ActionMateriel(idE,idN,idG);
            }
            return  all;
        }catch (Exception e){}finally {
            c.close();
        }
        return null;
    }
}
