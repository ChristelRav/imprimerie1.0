package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Specialite  {
    int idSpecialite;   String specialite; int salaire;
    public int getidSpecialite() {return idSpecialite;}    public void setidSpecialite(int idSpecialite) {this.idSpecialite = idSpecialite;}
    public String getspecialite() {return specialite;}    public void setspecialite(String specialite) {this.specialite = specialite;}

    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }
    public Specialite(){}
    public Specialite(String s){this.setspecialite(s);}
    public Specialite(int idSpecialite , String specialite){
        this.setidSpecialite(idSpecialite);     this.setspecialite(specialite);
    }
    public Specialite(int idSpecialite , String specialite ,int salaire){
        this.setidSpecialite(idSpecialite);     this.setspecialite(specialite); this.setSalaire(salaire);
    }
    public Vector<Specialite> getSpecialite(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Specialite> all = new Vector<Specialite>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from specialite");
            while(res.next() ){
                int idS=res.getInt(1);
                String str=res.getString(2);
                all.add(new Specialite(idS,str));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
    public Vector<Specialite> getAllSpecialite(Connection c,int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Specialite> all = new Vector<Specialite>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from specialite where idSpecialite = "+id+" ");
            while(res.next() ){
                int idS=res.getInt(1);
                String str=res.getString(2);
                all.add(new Specialite(idS,str));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
}
