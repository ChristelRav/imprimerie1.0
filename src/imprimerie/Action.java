package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Action {
    int idAction; String action;
    int prix;

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public Action(String action, int prix) {
        this.action = action;
        this.prix = prix;
    }

    public Action(int idAction, String action, int prix) {
        this.idAction = idAction;
        this.action = action;
        this.prix = prix;
    }

    public  Action(){}
    public Action(int idAction, String action) {
        this.idAction = idAction;
        this.action = action;
    }
    public Action(String action) {
        this.action = action;
    }
    public Vector<Action> getAction(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Action> all = new Vector<Action>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from action");
            while(res.next() ){
                int idN=res.getInt(1);
                String str=res.getString(2);
                int prix = res.getInt(3);
                all.add(new Action(idN,str,prix));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
    public Vector<Action> getAllAction(Connection c,int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Action> all = new Vector<Action>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from action where idAction ="+id+"");
            while(res.next() ){
                int idN=res.getInt(1);
                String str=res.getString(2);
                int prix = res.getInt(3);
                all.add(new Action(idN,str,prix));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
}
