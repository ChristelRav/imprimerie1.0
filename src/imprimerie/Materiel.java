package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Materiel {
    int idMateriel, materiel,prix;

    public int getIdMateriel() {
        return idMateriel;
    }

    public void setIdMateriel(int idMateriel) {
        this.idMateriel = idMateriel;
    }

    public int getMateriel() {
        return materiel;
    }

    public void setMateriel(int materiel) {
        this.materiel = materiel;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public Materiel() {}

    public Materiel(int materiel, int prix) {
        this.materiel = materiel;
        this.prix = prix;
    }
    public Materiel(int idMateriel, int materiel, int prix) {
        this.idMateriel = idMateriel;
        this.materiel = materiel;
        this.prix = prix;
    }
    public Vector<Materiel> getAllSpecialiteEmploye(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Materiel> all = new Vector<Materiel>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from Materiel");
            while(res.next() ){
                int idS=res.getInt(1);
                int idE=res.getInt(2);
                int prix = res.getInt(3);
                all.add(new Materiel(idS,idE,prix));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }

}
