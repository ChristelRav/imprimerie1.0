package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Niveau {
    int idNiveau;   String niveau;
    public int getidNiveau() {return idNiveau;}    public void setidNiveau(int idNiveau) {this.idNiveau = idNiveau;}
    public String getniveau() {return niveau;}    public void setniveau(String niveau) {this.niveau = niveau;}
    public Niveau(){}
    public Niveau(String nv){this.setniveau(nv);}
    public Niveau(int idNiveau , String nv){
        this.setidNiveau(idNiveau); this.setniveau(nv);
    }
    public Vector<Niveau> getAllNiveau(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Niveau> all = new Vector<Niveau>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from niveau");
            while(res.next() ){
                int idN=res.getInt(1);
                String str=res.getString(2);
                all.add(new Niveau(idN,str));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
    public void insertNiveau(Connection c)throws Exception{
        try {
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO   "+this.getClass().getSimpleName()+" (niveau) VALUES ('"+this.getniveau()+"')";
            System.out.println(sql);
            stmt.executeUpdate(sql);
        }catch (Exception e){}
    }
}

