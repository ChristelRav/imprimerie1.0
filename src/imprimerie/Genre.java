package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Genre {
    int idGenre;      String genre;
    public int getidGenre() {
        return idGenre;
    }    public void setidGenre(int idGenre) {
        this.idGenre = idGenre;
    }
    public String getgenre() {
        return genre;
    }    public void setgenre(String genre) {
        this.genre = genre;
    }
    public Genre() {}
    public Genre(String genre) {this.setgenre(genre);    }
    public Genre(int idGenre, String genre) {
        this.setidGenre(idGenre);
        this.setgenre(genre);
    }
    public Vector<Genre> getAllGenre(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Genre> all = new Vector<Genre>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from genre");
            while(res.next() ){
                int idG=res.getInt(1);
                String str=res.getString(2);
                all.add(new Genre(idG,str));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
}