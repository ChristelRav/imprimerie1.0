package imprimerie;


import connect.Connexion;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Vector;
public class Employe extends DatabaseObject{
    int idEmploye;
    String  nom;
    String prenom;
    Date dateNaissance;
    int idNiveau;
    int idGenre;

    public Employe() {

    }


    //Setters......................
    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDateNaissance(Date dateNaissance) throws Exception {
        LocalDate today=LocalDate.now();
        int age= Period.between(dateNaissance.toLocalDate(),today).getYears();
        if(age<18){
            throw new Exception("date de naissance non valide");
        }
        else{
            this.dateNaissance = dateNaissance;
        }
    }

    public void setIdNiveau(int idNiveau) {
        this.idNiveau = idNiveau;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }

    public Employe(String nom, String prenom, Date dateNaissance, int idNiveau, int idGenre) throws Exception {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setDateNaissance(dateNaissance);
        this.setIdNiveau(idNiveau);
        this.setIdGenre(idGenre);
    }
    public Employe(int idEmploye,String nom, String prenom, Date dateNaissance, int idNiveau, int idGenre) throws Exception {
        this.setIdEmploye(idEmploye);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setDateNaissance(dateNaissance);
        this.setIdNiveau(idNiveau);
        this.setIdGenre(idGenre);
    }

    //Getters.........................
    public int getIdEmploye() {
        return idEmploye;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public int getIdNiveau() {
        return idNiveau;
    }

    public int getIdGenre() {
        return idGenre;
    }
    //get all employe.................
    public Vector<Employe> getAll(Connection c) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        List<Object> totalite=this.getList(c);
        Vector<Employe> allEmploye=new Vector<Employe>();
        for(int i=0;i<totalite.size();i++){
            Employe intermediaire=(Employe) totalite.get(i);
            allEmploye.add(intermediaire);
        }
        return allEmploye;
    }

    //insertion new employe............
    public void newEmploye(Connection c) throws SQLException, InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        this.save(c);
    }
    public Employe getOneCurrent(Connection c)throws Exception{
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Employe all = new Employe();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from "+this.getClass().getSimpleName()+ " order by idemploye desc limit 1 ");
            while(res.next() ){
                int idE=res.getInt(1);      String str=res.getString(2);      String str2=res.getString(3);
                Date dt = res.getDate(4);    int idN=res.getInt(5);      int idG=res.getInt(6);
                all = new Employe(idE,str,str2,dt,idN,idG);
            }
            return  all;
        }catch (Exception e){}finally {
            c.close();
        }
        return null;
    }
}
