package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class BeneficeMateriel {
    int idAction;
    String materiel;
    int prix,quantite;

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }

    public String getMateriel() {
        return materiel;
    }

    public void setMateriel(String materiel) {
        this.materiel = materiel;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public BeneficeMateriel() {}
    public BeneficeMateriel(int idAction, String materiel, int prix, int quantite) {
        this.idAction = idAction;
        this.materiel = materiel;
        this.prix = prix;
        this.quantite = quantite;
    }
    public Vector<BeneficeMateriel> getAllBeneficeMateriel(Connection c, int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<BeneficeMateriel> all = new Vector<BeneficeMateriel>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from beneficeMateriel where idAction ="+id+"");
            while(res.next() ){
                int idN=res.getInt(1);
                String str=res.getString(2);
                int prix = res.getInt(3);
                int qtt=res.getInt(4);
                all.add(new BeneficeMateriel(idN,str,prix,qtt));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
    public int revientMat(Connection c, int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            int rv = 0;
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select sum(prix*quantite) from beneficeMateriel where idAction ="+id+"");
            while(res.next() ){
                int idN=res.getInt(1);
               rv = idN;
            }
            return  rv;
        }catch (Exception e){}
        return 0;
    }
}
