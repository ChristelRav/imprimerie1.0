package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class SpecialiteEmploye {
        int idSpecialite , idEmploye;
    public int getidSpecialite() {return idSpecialite;}    public void setidSpecialite(int idSpecialite) {this.idSpecialite = idSpecialite;}
    public int getidEmploye() {return idEmploye;}           public void setidEmploye(int idEmploye) {this.idEmploye = idEmploye;}
    public SpecialiteEmploye(){}
    public SpecialiteEmploye(int idSpecialite , int idEmploye){
        this.setidSpecialite(idSpecialite); this.setidEmploye(idEmploye);
    }
    public Vector<SpecialiteEmploye> getAllSpecialiteEmploye(Connection c){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<SpecialiteEmploye> all = new Vector<SpecialiteEmploye>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from specialiteEmploye");
            while(res.next() ){
                int idS=res.getInt(1);
                int idE=res.getInt(2);
                all.add(new SpecialiteEmploye(idS,idE));
            }
            return  all;
        }catch (Exception e){}
       return null;
    }
    public void insertSpecialiteEmploye(Connection c){
        try {
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO   "+this.getClass().getSimpleName()+" (idspecialite , idemploye) VALUES ("+this.getidSpecialite()+","+this.getidEmploye()+")";
            System.out.println(sql);
            stmt.executeUpdate(sql);
        }catch (Exception e){}
    }
}
