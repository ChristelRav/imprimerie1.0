package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class ActionSpecialite {
    int idAction , idSpecialite ;
    double heureTravail;

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }

    public int getIdSpecialite() {
        return idSpecialite;
    }

    public void setIdSpecialite(int idSpecialite) {
        this.idSpecialite = idSpecialite;
    }

    public double getHeureTravail() {
        return heureTravail;
    }

    public void setHeureTravail(double heureTravail) {
        this.heureTravail = heureTravail;
    }

    public ActionSpecialite() {
    }

    public ActionSpecialite(int idAction, int idSpecialite, double heureTravail) {
        this.idAction = idAction;
        this.idSpecialite = idSpecialite;
        this.heureTravail = heureTravail;
    }
    public Vector<ActionSpecialite> getAllActionSpecialite(Connection c , int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<ActionSpecialite> all = new Vector<ActionSpecialite>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from    ActionSpecialite where idAction ="+id+" ");
            while(res.next() ){
                int idN=res.getInt(1);
               int idS=res.getInt(2);
               double ht = res.getDouble(3);
                all.add(new ActionSpecialite(idN,idS,ht));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }

}
