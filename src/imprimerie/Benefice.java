package imprimerie;

import connect.Connexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Benefice {
    int idAction;
    String specialite;
    int salaire;
    double heureTravail;
    double total ;

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public int getSalaire() {
        return salaire;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public double getHeureTravail() {
        return heureTravail;
    }

    public void setHeureTravail(double heureTravail) {
        this.heureTravail = heureTravail;
    }

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }

    public Benefice(int idAction, String specialite, int salaire, double heureTravail) {
        this.idAction = idAction;
        this.specialite = specialite;
        this.salaire = salaire;
        this.heureTravail = heureTravail;
    }

    public Benefice() {
    }

    public Benefice(String specialite, int salaire, double heureTravail) {
        this.specialite = specialite;
        this.salaire = salaire;
        this.heureTravail = heureTravail;
    }

    public Benefice(int idAction, String specialite, int salaire, double heureTravail, double total) {
        this.idAction = idAction;
        this.specialite = specialite;
        this.salaire = salaire;
        this.heureTravail = heureTravail;
        this.total = total;
    }

    public Vector<Benefice> getAllBenefice(Connection c, int id){
        try{
            if (c == null) {
                Connexion k = new Connexion();
                c = k.seConnecterPostgres();
            }
            Vector<Benefice> all = new Vector<Benefice>();
            Statement stmt = c.createStatement();
            ResultSet res = stmt.executeQuery("select * from Benefice where idAction = "+id+" ");
            while(res.next() ){
                int idS=res.getInt(1);
                String str=res.getString(2);
                int sal=res.getInt(3);
                double ht = res.getDouble(4);
                double ttl =  (sal/240)*ht;
                all.add(new Benefice(idS,str,sal,ht,ttl));
            }
            return  all;
        }catch (Exception e){}
        return null;
    }
}

