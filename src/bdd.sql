kcreate database imprimerie;
alter database imprimerie owner to postgres;

create table specialite(
    idSpecialite serial primary key,
    specialite varchar(20),
    salaire int
);
insert into specialite(specialite, salaire) values('Nettoyage', 200000);
insert into specialite(specialite, salaire) values('Securite', 300000);
insert into specialite(specialite, salaire) values('Saisie', 400000);
insert into specialite(specialite, salaire) values('Conception', 500000);
insert into specialite(specialite, salaire) values('Design', 600000);

create table niveau(
    idNiveau serial primary key,
    niveau varchar(20)
);
insert into niveau(niveau) values('CEPE');
insert into niveau(niveau) values('BEPC');
insert into niveau(niveau) values('BACC');
insert into niveau(niveau) values('LICENCE');
insert into niveau(niveau) values('MASTER 1');
insert into niveau(niveau) values('MASTER 2');

create table genre(
    idGenre serial primary key,
    genre varchar(20)
);
insert into genre(genre) values('Homme');
insert into genre(genre) values('Femme');

create table employe(
    idEmploye serial primary key,
    nom varchar(20),
    prenom varchar(20),
    dateNaissance date,
    idNiveau int,
    idGenre int,
    foreign key(idNiveau) references niveau(idNiveau),
    foreign key(idGenre) references genre(idGenre)
);
create table specialiteEmploye(
    idSpecialite int,
    idEmploye int,
    foreign key(idSpecialite) references specialite(idSpecialite),
    foreign key(idEmploye) references employe(idEmploye)
);
create table action(
    idAction serial primary key,
    action varchar(20),
    prix int
);
insert into action(action, prix) values ('livres', 45000);
insert into action(action, prix) values ('affiche', 30000);
insert into action(action, prix) values ('carte de visite', 20000);
create table actionSpecialite(
    idAction int references action(idAction),
    idSpecialite int references specialite(idSpecialite),
    heureTravail double precision
);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (2, 3, 1);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (3, 4, 2);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (1, 2, 4);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (2, 1, 5);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (1, 3, 3);
insert into actionSpecialite(idAction, idSpecialite, heureTravail) values (3, 5, 1);

create or replace view benefice as (select idAction ,s.specialite,s.salaire,heureTravail from actionSpecialite
    join specialite s on s.idSpecialite = actionSpecialite.idSpecialite
 );
create table  materiel (
    idMateriel serial primary key,
    materiel varchar(50),
    prix int
);
insert into materiel (materiel,prix) values ('feuille',500);
insert into materiel (materiel,prix) values ('encre',500);
insert into materiel (materiel,prix) values ('imprimante',10000);
insert into materiel (materiel,prix) values ('ordinateur',15000);

create table  actionMateriel (
        idaction int,
        idmateriel int,
        quantite int,
        foreign key(idAction) references action(idAction),
        foreign key(idmateriel) references materiel(idmateriel)
);

insert into actionMateriel (idaction, idmateriel,quantite) values (1,1,150);
insert into actionMateriel (idaction, idmateriel,quantite) values (2,2,50);
insert into actionMateriel (idaction, idmateriel,quantite) values (1,3,2);
insert into actionMateriel (idaction, idmateriel,quantite) values (1,3,5);
insert into actionMateriel (idaction, idmateriel,quantite) values (1,2,40);
insert into actionMateriel (idaction, idmateriel,quantite) values (1,2,20);

create or replace view beneficeMateriel as (select idAction ,s.materiel,s.prix,count(s.idMateriel) from actionMateriel
    join materiel s on s.idMateriel = actionMateriel.idmateriel
 );

select count(materiel) from beneficeMateriel;

create SommeBenefice as (select sum(prix*quantite),idaction from beneficeMateriel) ;






