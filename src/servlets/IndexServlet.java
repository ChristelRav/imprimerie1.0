package servlets;

import imprimerie.Action;
import imprimerie.Genre;
import imprimerie.Niveau;
import imprimerie.Specialite;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Vector;

@WebServlet(name = "IndexServlet", value = "/IndexServlet")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Genre gr = new Genre();
        Vector<Genre> vg = gr.getAllGenre(null);
        request.setAttribute("genre",vg);

        Niveau nv = new Niveau();
        Vector<Niveau> vnv = nv.getAllNiveau(null);
        request.setAttribute("niveau",vnv);

        Specialite sp = new Specialite();
        Vector< Specialite> vsp = sp.getSpecialite(null);
        request.setAttribute("specialite",vsp);

        RequestDispatcher dispat = request.getRequestDispatcher("/insert.jsp");
        dispat.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
