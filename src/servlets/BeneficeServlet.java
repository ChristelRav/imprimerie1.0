package servlets;

import imprimerie.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

@WebServlet(name = "BeneficeServlet", value = "/BeneficeServlet")
public class BeneficeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try {
           response.setContentType("text/plain");
           PrintWriter out = response.getWriter();

           int idAction = Integer.parseInt(request.getParameter("action"));
           Vector<Action> va = new Action().getAllAction(null,idAction);
           int prix = va.get(0).getPrix() ;

           double som = 0;
           Vector<Benefice> vas = new Benefice().getAllBenefice(null,idAction);
           for (int i = 0; i < vas.size(); i++) {
               som += vas.get(i).getTotal() ;
           }

            out.println("Aaaaaa");
           double revient = prix - som;
            out.println(revient +" , "+som );
           ServletContext result = this.getServletContext();
           request.setAttribute("prix",prix);
           request.setAttribute("acts",va);
           out.println("Aaaaaa");
           request.setAttribute("benefice",vas);
           request.setAttribute("revient",revient);
           out.println("Aaaaaa");

           int rvm =  new BeneficeMateriel().revientMat(null,idAction);
           Vector<BeneficeMateriel> bm = new BeneficeMateriel().getAllBeneficeMateriel(null,idAction);
           request.setAttribute("revientMateriel",rvm);
           request.setAttribute("beneficeM",bm);

          RequestDispatcher dispat = request.getRequestDispatcher("/Revient.jsp");
          dispat.forward(request,response);
       }catch (Exception e){}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
