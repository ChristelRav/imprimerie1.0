package servlets;

import connect.Connexion;
import imprimerie.Action;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Vector;

@WebServlet(name = "AccueilServlet", value = "/AccueilServlet")
public class AccueilServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try {
           response.setContentType("text/plain");
           PrintWriter out = response.getWriter();

           Connexion connex=new Connexion();
           Connection c=connex.seConnecterPostgres();
           ServletContext result = this.getServletContext();

           Action a = new Action();
           Vector<Action> va = a.getAction(c);
           request.setAttribute("actions",va);

           RequestDispatcher dispat = request.getRequestDispatcher("/Accueil.jsp");
           dispat.forward(request,response);
       }catch (Exception e){}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
