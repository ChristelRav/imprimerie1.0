package servlets;

import connect.Connexion;
import imprimerie.Employe;
import imprimerie.SpecialiteEmploye;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;


@WebServlet(name = "ImprimerieServlet", value = "/ImprimerieServlet")
public class ImprimerieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println("SERVLET = ImprimerieInsert");

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String dtn = request.getParameter("dtn");
        String genre = request.getParameter("genre");
        String niveau = request.getParameter("niveau");
        String[] specialite = request.getParameterValues("specialite[]");
        out.println(nom+" , "+prenom+" , "+dtn+" , "+genre+" , "+niveau);


        int nv = Integer.parseInt(niveau);
        int g = Integer.parseInt(genre);
        Date dateNaiss = Date.valueOf(dtn);

        try {

            Connexion connex=new Connexion();
            Connection c=connex.seConnecterPostgres();
            System.out.println(c);
                Employe emp = new Employe(nom,prenom,dateNaiss,nv,g);
                emp.save(c);
                Employe ec = new Employe().getOneCurrent(null);
            for (int i = 0; i < specialite.length; i++) {
                out.println("LES SPECIALITE = "+specialite[i]);
                int a = Integer.parseInt(specialite[i]);
                SpecialiteEmploye spe =new SpecialiteEmploye(a,ec.getIdEmploye());
                spe.insertSpecialiteEmploye(null);
            }
                out.println("AAAA");
        } catch (Exception e) {
            out.println(e.getMessage());
        }
//        emp.save(c);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
