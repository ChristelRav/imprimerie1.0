package connect;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connexion {
    public Connection seConnecterPostgres()throws Exception{
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/imprimerie", "postgres", "mdpprom15");
        return conn;
    }
}
