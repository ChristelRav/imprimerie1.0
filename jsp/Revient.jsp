<%--
  Created by IntelliJ IDEA.
  User: ravmi
  Date: 17/01/2023
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.*" %>
<%@ page import="imprimerie.Benefice" %>
<%@ page import="imprimerie.Action" %>
<%@ page import="imprimerie.BeneficeMateriel" %>
<% int prix = (int)request.getAttribute("prix"); %>
<% int rv = (int)request.getAttribute("revientMateriel"); %>
<%    double revient = (double)request.getAttribute("revient");%>
<%    Vector<Benefice> benefice = (Vector<Benefice>)request.getAttribute("benefice");%>
<%    Vector<BeneficeMateriel> beneficeM = (Vector<BeneficeMateriel>)request.getAttribute("beneficeM");%>
<%    Vector<Action> action = (Vector<Action>)request.getAttribute("acts");%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h3><% out.print(action.get(0).getAction());%></h3>
    <p>Prix : <% out.print(prix); %></p>
    <h4>Revient Salaire</h4>
    <table border="1">
    <tr>
        <th></th>
        <th>Salaire</th>
        <th>heure Travail</th>
        <th>Total</th>
    </tr>

        <% for (int i = 0; i<benefice.size();i++) {%>
    <tr>
        <td><% out.print(benefice.get(i).getSpecialite()); %></td>
        <td><% out.print(benefice.get(i).getSalaire()); %></td>
        <td><% out.print(benefice.get(i).getHeureTravail()); %></td>
        <td><% out.print(benefice.get(i).getTotal());  %></td>
    </tr>
        <% } %>

</table>
<p>Revient : <% out.print(revient); %></p>

<h4>Revient materiel :</h4>

    <table border="1">
        <tr>
            <th>Materiel</th>
            <th>prix</th>
            <th>quantite</th>
            <th>Total</th>
        </tr>
        <% for (int i = 0; i<beneficeM.size();i++) {%>
        <tr>
            <td><% out.print(beneficeM.get(i).getMateriel()); %></td>
            <td><% out.print(beneficeM.get(i).getPrix()); %></td>
            <td><% out.print(beneficeM.get(i).getQuantite()); %></td>
            <% int ttl = beneficeM.get(i).getPrix()*beneficeM.get(i).getQuantite(); %>
            <td><% out.print(ttl);  %></td>
        </tr>
        <% } %>
        <p> Total : <% out.print(rv);  %>
    </table>
</html>
