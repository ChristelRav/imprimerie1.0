<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.*" %>
<%@ page import="imprimerie.Genre" %>
<%@ page import="imprimerie.Niveau" %>
<%@ page import="imprimerie.Specialite" %>
<% Vector<Genre> gr = (Vector<Genre>) request.getAttribute("genre"); %>
<% Vector<Niveau> nv = (Vector<Niveau>) request.getAttribute("niveau"); %>
<% Vector<Specialite> sp = (Vector<Specialite>) request.getAttribute("specialite"); %>
<!DOCTYPE html>
<html>
<head>
    <title>INSERT PAGE OF IMPRIMERIE</title>
</head>
<body>
<h3>CYBER Cafe</h3>
<p style="text-decoration: underline">Formulaire d'insertion Employe :</p>
<br/>
<form action="${pageContext.request.contextPath}/ImprimerieServlet" method="get">
    <p><label for = "name">Entrez votre Nom:</label>
        <input type="text" name="nom" id = "name" /></p>
    <p><label for = "id">Entrez votre Prenom:</label>
        <input type="text" name="prenom" id = "id" /></p>
    <p><label for = "id">Entrez votre Date de naissance</label>
        <input type="date" name="dtn" id = "dtn" /></p>

    <p><label>Votre Genre :</label>
        <select name="genre">
            <% for (int i = 0; i < gr.size(); i++) { %>
                     <option value="<% out.print(gr.get(i).getidGenre()); %>" ><% out.print(gr.get(i).getgenre()); %></option>

            <% } %>
        </select></p>

    <p><label>Selectionnez votre niveau d'etude :</label>
        <select name="niveau">
            <option value="3" >Licence</option>
            <% for (int i = 0; i < nv.size(); i++) { %>
            <option value="<% out.print(nv.get(i).getidNiveau()); %>" ><% out.print(nv.get(i).getniveau()); %></option>

            <% } %>
        </select></p>

    <p><label>Cochez la specialite ou les specialites de votre choix :</label>
        <% for(int i = 0; i < sp.size() ; i++) { %>
            <p><input type="checkbox" id="coding" name="specialite[]" value="<%out.print(sp.get(i).getidSpecialite());%>">
            <label for="<%out.print(sp.get(i).getidSpecialite());%>"><%out.print(sp.get(i).getspecialite());%></label></p>
        <% } %>
    <button>Inserer</button>
</form>
<a href="${pageContext.request.contextPath}/AccueilServlet">Accueil </a>
</body>
</html>